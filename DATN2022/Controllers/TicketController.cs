﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        public TicketController(IMapper mapper, DATN2022DbContext context)
        {

            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TicketDto>>> GetAll([FromQuery] string tenantId)
        {
            var datas = await _context.Tickets.Where(t => t.TenantId == tenantId && t.Status ==1).ToListAsync();
            if (datas.Count == 0)
            {
                return NoContent();
            }
            return Ok(_mapper.Map<IEnumerable<TicketDto>>(datas));

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TicketDto>> GetById(Guid id)
        {
            var data = await _context.Tickets.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<TicketDto>(data));
        }

        [HttpPost]
        public async Task<Object> Create(TicketDto ticket)
        {
            try
            {
                var entity = _mapper.Map<Ticket>(ticket);
                var result = _context.Tickets.Add(entity);
                if (result != null)
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return false;
            }
        }


        [HttpPut]
        public async Task<ActionResult> Update(TicketDto ticket)
        {
            var entity = _mapper.Map<Ticket>(ticket);
            var result = _context.Tickets.Update(entity);
            if (result != null)
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    return NotFound();
                }

                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]

        public async Task<ActionResult<Ticket>> Delete(Guid id)
        {

            var data = await _context.Tickets.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                _context.Tickets.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }
        //private bool isExists(Guid id)
        //{
        //    return _context.Tickets.Any(e => e.Id == id && !e.IsDeleted);
        //}
    }
}
