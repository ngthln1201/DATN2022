﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LayoutController : ControllerBase
    {
        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        public LayoutController(IMapper mapper, DATN2022DbContext context)
        {

            _mapper = mapper;
            _context = context;
        }

        [HttpGet("Getall")]
        public async Task<ActionResult<IEnumerable<LayoutDto>>> GetAll()
        {
            var datas = await _context.Layouts.Where(x => !x.IsDeleted).ToListAsync();
            return Ok(_mapper.Map<IEnumerable<LayoutDto>>(datas));

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<LayoutDto>> GetById(Guid id)
        {
            var data = await _context.Layouts.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<LayoutDto>(data));
        }


        [HttpPost("Create")]
        public async Task<Object> Create(LayoutDto layout)
        {
            try
            {
                var entity = _mapper.Map<Layout>(layout);
                var result = _context.Layouts.Add(entity);
                if (result != null)
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return false;
            }
        }
        [HttpPut("Update/{id}")]
        public async Task<ActionResult> Update(LayoutDto layout, Guid id)
        {
            var record = await _context.Layouts.FindAsync(id);
            if (record == null)
            {
                return null;
            }
            record.NumberOfDeckers = layout.NumberOfDeckers;
            record.Value = layout.Value;
            record.Name = layout.Name;
            var result = _context.Layouts.Update(record);
            if (result != null)
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    if (!isExists(layout.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("Delete/{id}")]

        public async Task<ActionResult<Layout>> Delete(Guid id)
        {

            var data = await _context.Layouts.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                _context.Layouts.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }

        private bool isExists(Guid id)
        {
            return _context.Layouts.Any(e => e.Id == id && !e.IsDeleted);
        }
    }
}
