﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Dtos.Request;
using DATN2022.Models;
using DATN2022.Services.Implements;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using Type = DATN2022.Models.Type;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoachController : ControllerBase
    {

        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        public CoachController(IMapper mapper, DATN2022DbContext context)
        {
            _mapper = mapper;
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CoachDto>>> GetAll([FromQuery] string tenantId)
        {
            var datas = await _context.Coaches.Where(x => x.TenantId == tenantId && !x.IsDeleted && x.Status != 0 ).ToListAsync();
            return datas.Count <= 0 ? NoContent() : Ok(_mapper.Map<IEnumerable<CoachDto>>(datas));

        }
        [Route("get-all-by-route")]
        [HttpPost]
        public async Task<ActionResult<IEnumerable<Coach>>> GetAllByRoute([FromBody] GetAllByRouteRequestModel input)
        {
            //tìm chuyến đi
            var time = DateTime.Parse(input.Departuretime.ToString("yyyy/MM/dd"));
            var datas = await _context.Trips.Where(x => !x.IsDeleted && x.From.Equals(input.From) && x.To.Equals(input.To)).ToListAsync();
            var coaches = from c in _context.Coaches
                          join t in _context.Trips
                          on c.TripId equals t.Id
                          where t.From.Equals(input.From) && t.To.Equals(input.To) && c.DepartureTime >= time && c.DepartureTime < time.AddDays(1) && !t.IsDeleted
                          select new
                          {
                              Id = c.Id,
                              CoachOwner = _context.CoachOwners.Where(x => x.Id == c.CoachOwnerId && !x.IsDeleted).Select(x => x.FirstName + x.LastName).FirstOrDefault(),
                              //Image=c.Image,
                              DepartureTime = c.DepartureTime,
                              NumberOfSeats = _context.Seats.Count(x => x.CoachId == c.Id && !x.IsDeleted),
                              EmptySeats = _context.Seats.Count(x => x.CoachId == c.Id && x.Status != 0 && !x.IsDeleted),
                              TicketPrice = c.Price,
                              Pickups = _context.Pickups.Where(x => x.CoachId == c.Id && !x.IsDeleted).ToList(),
                              DropOffs = _context.DropOffs.Where(x => x.CoachId == c.Id && !x.IsDeleted).ToList(),
                          };


            return (datas.Count <= 0) ? Ok(null) : Ok(coaches);

        }

        [Route("get-coach-detail")]
        [HttpPost]
        public ActionResult<Coach> GetCoachDetail([FromBody] GetCoachDetailsRequest input)
        {
            var coaches = from c in _context.Coaches
                          where c.Id == input.CoachId && !c.IsDeleted
                          select new
                          {
                              Id = c.Id,
                              Reverse = c.Reserve,
                              Price = c.Price,
                              Pickups = _context.Pickups.Where(x => x.CoachId == c.Id && !x.IsDeleted).ToList(),
                              DropOffs = _context.DropOffs.Where(x => x.CoachId == c.Id && !x.IsDeleted).ToList(),
                              Seats = _context.Seats.Where(x => x.CoachId == c.Id && !x.IsDeleted).OrderBy(x => x.SeatNo).ToList(),
                          };

            return Ok(coaches);
        }
        [Route("GetAllSeatsById")]
        [HttpGet]
        public async Task<ActionResult<List<SeatDto>>> GetAllSeatsById(Guid id)
        {
            var datas = await _context.Seats.Where(x => !x.IsDeleted && x.CoachId == id).OrderBy(x => x.SeatNo).ToListAsync();
            return datas.Count <= 0 ? NotFound() : Ok(_mapper.Map<List<SeatDto>>(datas));
        }

        [HttpGet("GetOwner/{id}")]
        public async Task<ActionResult<CoachOwnerDto>> GetOwner(Guid id)
        {
            var data = await _context.CoachOwners.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<CoachOwnerDto>(data));
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<CoachDto>> GetById(Guid id)
        {
            var data = await _context.Coaches.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<CoachDto>(data));
        }
        private bool isDuplicateTrip(string from, string to)
        {
            return _context.Trips.Any(e => e.From == from && e.To == to && !e.IsDeleted);
        }

        [HttpPost]
        public async Task<Object> Create(CreateCoachDto createCoach)
        {
            try
            {
                if (isDuplicateTrip(createCoach.From, createCoach.To))
                {
                    var tripData = _context.Trips.Where(x => !x.IsDeleted && x.From.Equals(createCoach.From) && x.To.Equals(createCoach.To));
                    Trip tripInfo = _mapper.Map<Trip>(tripData);

                    Coach coachEntity = _mapper.Map<Coach>(
                    new CoachDto(createCoach.Id, createCoach.DepartureTime, createCoach.LisencePlates, createCoach.Reserve, createCoach.Color, createCoach.Price, tripInfo.Id, createCoach.TypeId, createCoach.isPrepayment, createCoach.CoachOwnerId, createCoach.TenantId , 1)
                    );

                    var coachResult = _context.Coaches.Add(coachEntity);

                    foreach (var p in createCoach.pickups)
                    {
                        Pickup pickupEntity = _mapper.Map<Pickup>(new PickupDto(Guid.NewGuid(), p.PhoneNumber, p.Address, coachEntity.Id));
                        _context.Pickups.Add(pickupEntity);
                    }
                    foreach (var p in createCoach.dropOffs)
                    {
                        DropOff dropOffEntity = _mapper.Map<DropOff>(new PickupDto(Guid.NewGuid(), p.PhoneNumber, p.Address, coachEntity.Id));
                        _context.DropOffs.Add(dropOffEntity);
                    }
             
                    if (coachResult != null)
                    {
                        var datas = _context.Types.Where(x => x.Id == coachEntity.TypeId && !x.IsDeleted).Select(x => x.Seats).FirstOrDefault();
                        for (int i = 1; i <= datas; i++)
                        {
                            SeatDto seatdto = new SeatDto()
                            {
                                Id = Guid.NewGuid(),
                                SeatNo = i,
                                Status = 1,
                                CustomPrice = 130000,
                                CoachId = coachEntity.Id,
                            };
                            var seatEn = _mapper.Map<Seat>(seatdto);
                            var seatresult = _context.Seats.Add(seatEn);
                        }
                        await _context.SaveChangesAsync();

                        return CreatedAtAction("GetById", new { id = coachEntity.Id }, coachEntity);

                    }
                }
                else
                {
                    Trip tripEntity = _mapper.Map<Trip>(new TripDto(Guid.NewGuid(), createCoach.From, createCoach.To, DateTime.Now.Ticks.ToString()));
                    Coach coachEntity = _mapper.Map<Coach>(
                    new CoachDto(createCoach.Id, createCoach.DepartureTime, createCoach.LisencePlates, createCoach.Reserve, createCoach.Color, createCoach.Price, tripEntity.Id, createCoach.TypeId, createCoach.isPrepayment, createCoach.CoachOwnerId, createCoach.TenantId, 1)
                    );

                    var tripResult = _context.Trips.Add(tripEntity);
                    var coachResult = _context.Coaches.Add(coachEntity);

                    foreach (var p in createCoach.pickups)
                    {
                        Pickup pickupEntity = _mapper.Map<Pickup>(new PickupDto(Guid.NewGuid(), p.PhoneNumber, p.Address, coachEntity.Id));
                        _context.Pickups.Add(pickupEntity);
                    }
                    foreach (var p in createCoach.dropOffs)
                    {
                        DropOff dropOffEntity = _mapper.Map<DropOff>(new DropOffDto(Guid.NewGuid(), p.PhoneNumber, p.Address, coachEntity.Id, createCoach.TenantId, 1));
                        _context.DropOffs.Add(dropOffEntity);
                    }

                    if (coachResult != null)
                    {
                        var datas = _context.Types.Where(x => x.Id == coachEntity.TypeId && !x.IsDeleted).Select(x => x.Seats).FirstOrDefault();
                        for (int i = 1; i <= datas; i++)
                        {
                            SeatDto seatdto = new SeatDto()
                            {
                                Id = Guid.NewGuid(),
                                SeatNo = i,
                                Status = 1,
                                CustomPrice = 130000,
                                CoachId = coachEntity.Id,
                            };
                            var seatEn = _mapper.Map<Seat>(seatdto);
                            var seatresult = _context.Seats.Add(seatEn);
                        }
                        await _context.SaveChangesAsync();
                        return CreatedAtAction("GetById", new { id = coachEntity.Id }, coachEntity);

                    }
                }
                throw new Exception("Có lỗi xảy ra vui lòng thử lại!");
            }
            catch (Exception)
            {
                throw new Exception("Hệ thống đang bảo trì, vui lòng thử lại sau");
            }
        }


        [HttpPut]
        public async Task<ActionResult> Update(Guid id, CoachDto coach)
        {
            var coachEntity = _context.Coaches.FindAsync(id);
            if (coachEntity.Equals(null))
            {
                throw new Exception("Không tìm thấy xe!");
            }
            var entity = _mapper.Map<Coach>(coach);
            var result = _context.Coaches.Update(entity);
            if (result != null)
            {
                    await _context.SaveChangesAsync();
                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]

        public async Task<ActionResult<Coach>> Delete(Guid id)
        {

            var data = await _context.Coaches.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                _context.Coaches.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }
        private bool isExists(Guid id)
        {
            return _context.Coaches.Any(e => e.Id == id && !e.IsDeleted);
        }
        private Guid findByEmail(string email)
        {
           var result = _mapper.Map<CoachOwnerDto>(_context.CoachOwners.Where(e => e.Email == email && !e.IsDeleted));
            return result.Id;
        }

        //public UserLoginDto currentUser()
        //{
        //    var identity = HttpContext.User.Identity as ClaimsIdentity;
        //    if(identity != null)
        //    {
        //        var userClaims = identity.Claims;
        //        return new UserLoginDto
        //        {
        //            FirstName = userClaims.FirstOrDefault(x=>x.Type == ClaimTypes.GivenName)?.Value,
        //            LastName = userClaims.FirstOrDefault(x => x.Type == ClaimTypes.Surname)?.Value,
        //            Email = userClaims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value,
        //            Role = userClaims.FirstOrDefault(x => x.Type == ClaimTypes.Role)?.Value,
        //        };
        //    }
        //    return null;
        //}

        [HttpGet("info")]
        public async Task<List<Type>> GetInfoForCreateType()
        {
            var datas = await _context.Types.Where(x => !x.IsDeleted).ToListAsync();
            return datas;
        }
    }
}
