﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        public OrderController(IMapper mapper, DATN2022DbContext context)
        {

            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderDto>>> GetAll()
        {
            var datas = await _context.Orders.ToListAsync();
            if (datas.Count <= 0)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<IEnumerable<OrderDto>>(datas));

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<OrderDto>> GetById(Guid id)
        {
            var data = await _context.Orders.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<OrderDto>(data));
        }

        [HttpPost]
        public async Task<Object> Create(OrderDto order)
        {
            try
            {
                var entity = _mapper.Map<Order>(order);
                var result = _context.Orders.Add(entity);
                if (result != null)
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPut]
        public async Task<ActionResult> Update(OrderDto order)
        {
            var entity = _mapper.Map<Order>(order);
            var result = _context.Orders.Update(entity);
            if (result != null)
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    return NotFound();
                }

                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]

        public async Task<ActionResult<Order>> Delete(Guid id)
        {

            var data = await _context.Orders.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                _context.Orders.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }
    }
}
