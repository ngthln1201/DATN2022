﻿using DATN2022.Dtos;
using DATN2022.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private readonly IBServices _services;
        public BookingController(IBServices services)
        {
            _services  = services;
        }
        [HttpPost]
        [Route("save-to-database")]
        public async Task<Object> SaveToDataBase()
        {
            try
            {
               return await _services.saveToDb();
              
            }
            catch (Exception)
            {
                return false;
            }
        }
        [HttpPost]
        [Route("save-booked-data-to-redis")]
        public object saveBookingValueToRedis([FromBody]BookingDto data)
        {
            try
            {
                return _services.saveBookingValueToRedis(data);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
