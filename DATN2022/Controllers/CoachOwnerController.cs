﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using DATN2022.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoachOwnerController : ControllerBase
    {

        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        private readonly ICoachOwnerService ownerService;

        public CoachOwnerController(IMapper mapper, DATN2022DbContext context, ICoachOwnerService ownerService)
        {
            _mapper = mapper;
            _context = context;
            this.ownerService = ownerService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CoachOwnerDto>>> GetAll()
        {
            var datas = await _context.CoachOwners.Where(x => x.Status == 1 && !x.IsDeleted).ToListAsync();
            return datas.Count <= 0 ? NotFound() : Ok(_mapper.Map<IEnumerable<CoachOwnerDto>>(datas));

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CoachOwnerDto>> GetById(Guid id)
        {
            var data = await _context.CoachOwners.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<CoachOwnerDto>(data));
        }

        [HttpPost]
        public async Task<Object> Registry(CoachOwnerDto registry)
        {
            try
            {
                if (ownerService.isDuplicateCoachOwner(registry.Email))
                {
                    long time = DateTime.Now.Ticks;
                    string tenantId = Convert.ToString(time);
                    registry.TenantId = tenantId;
                    var entity = _mapper.Map<CoachOwner>(registry);
                    var result = _context.CoachOwners.Add(entity);
                    Tenant tenant = new Tenant(registry.FirstName + registry.LastName, tenantId, null, 1);
                    _context.Tenants.Add(tenant);
                    if (result != null)
                    {
                        await _context.SaveChangesAsync();
                        return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                    }
                    return BadRequest();
                }
                else
                {
                    throw new Exception("Hệ thống đã tồn tại email với email đã nhập! " + registry.Email);
                }

            }
            catch (Exception)
            {
                throw new Exception("Hệ thống đang bảo trì, vui lòng thử lại sau");
            }
        }


        [HttpPut]
        public async Task<ActionResult> Update(Guid id, CoachOwnerDto coachOwner)
        {
            var coachOwnerEntity = _context.CoachOwners.FindAsync(id);
            if (coachOwnerEntity.Equals(null))
            {
                throw new Exception("Không tìm thấy nhà xe!");
            }
            var entity = _mapper.Map<CoachOwner>(coachOwner);
            var result = _context.CoachOwners.Update(entity);
            if (result != null)
            {
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            throw new Exception("Cập nhật thất bại, vui lòng thử lại sau");
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<CoachOwner>> Delete(Guid id)
        {

            var data = await _context.CoachOwners.FindAsync(id);
            if (data == null)
            {
                throw new Exception("Không tìm thấy nhà xe: "+ id);
            }
            else
            {
                data.IsDeleted = true;
                data.Status = 0;
                _context.CoachOwners.Update(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }
    }
}
