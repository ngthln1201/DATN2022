﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PickupController : ControllerBase
    {
        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        public PickupController(IMapper mapper, DATN2022DbContext context)
        {

            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PickupDto>>> GetAll([FromQuery] string tenantId)
        {
            var datas = await _context.Pickups.Where(x => !x.IsDeleted && x.TenantId.Equals(tenantId) && x.Status == 1).ToListAsync();
            if (datas.Count <= 0)
            {
                return NoContent();
            }
            return Ok(_mapper.Map<IEnumerable<PickupDto>>(datas));

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PickupDto>> GetById(Guid id)
        {
            var data = await _context.Pickups.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<PickupDto>(data));
        }

        [HttpPost]
        public async Task<Object> Create(PickupDto pickup)
        {
            try
            {
                var entity = _mapper.Map<Pickup>(pickup);
                var result = _context.Pickups.Add(entity);
                if (result != null)
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPut]
        public async Task<ActionResult> Update(PickupDto pickup)
        {
            var entity = _mapper.Map<Pickup>(pickup);
            var result = _context.Pickups.Update(entity);
            if (result != null)
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    if (!isExists(pickup.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]

        public async Task<ActionResult<Pickup>> Delete(Guid id)
        {

            var data = await _context.Pickups.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                _context.Pickups.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }
        private bool isExists(Guid id)
        {
            return _context.Pickups.Any(e => e.Id == id && !e.IsDeleted);
        }
    }
}
