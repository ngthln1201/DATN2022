﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SeatController : ControllerBase
    {
  
        private readonly DATN2022DbContext _context;     
        private readonly IMapper _mapper;
        public SeatController( IMapper mapper, DATN2022DbContext context)
        {
            _mapper = mapper;
           _context = context;    
        }
         [HttpGet]
      
        [HttpGet("{id}")]
        public async Task<ActionResult<SeatDto>> GetById(Guid id)
        {
            var data = await _context.Seats.FindAsync(id);
            return data == null ? throw new Exception("Not found seat with id: "+id) : Ok(_mapper.Map<SeatDto>(data));
        }
        [HttpPost]
        public async Task<Object> Create(SeatDto seat)
        {
            try
            {
                var entity = _mapper.Map<Seat>(seat);
                var result =  _context.Seats.Add(entity);
                if (result != null)
                {                  
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return false;
            }
        }
        [HttpPut]
        public async Task<ActionResult> Update(SeatDto seat)
        {
            var entity = _mapper.Map<Seat>(seat);
            var result =  _context.Seats.Update(entity);
            if (result != null)
            {
                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    if (!isExists(seat.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
               
                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
       
        public async Task<ActionResult<Seat>> Delete(Guid id)
        {
     
            var data = await _context.Seats.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                 _context.Seats.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }
        private bool isExists(Guid id)
        {
            return _context.Seats.Any(e => e.Id == id && !e.IsDeleted);
        }
    }
}
