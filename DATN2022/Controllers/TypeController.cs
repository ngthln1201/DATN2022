﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypeController : ControllerBase
    {
        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        public TypeController(IMapper mapper, DATN2022DbContext context)
        {

            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TypeDto>>> GetAll()
        {
            var datas = await _context.Types.Where(x => !x.IsDeleted).ToListAsync();
            return Ok(_mapper.Map<IEnumerable<TypeDto>>(datas));

        }

        [HttpGet("info")]
        public async Task<List<Layout>> GetInfoForCreateType()
        {
            var datas = await _context.Layouts.Where(x => !x.IsDeleted).ToListAsync();
            return datas;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TypeDto>> GetById(Guid id)
        {
            var data = await _context.Types.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<TypeDto>(data));
        }

        [HttpPost("create")]
        public async Task<Object> Create(TypeDto type)
        {
            try
            {
                var entity = _mapper.Map<Models.Type>(type);
                var result = _context.Types.Add(entity);
                if (result != null)
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPut]
        public async Task<ActionResult> Update(TypeDto type)
        {
            var entity = _mapper.Map<Models.Type>(type);
            var result = _context.Types.Update(entity);
            if (result != null)
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    return NotFound();
                }

                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]

        public async Task<ActionResult<Models.Type>> Delete(Guid id)
        {

            var data = await _context.Types.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                _context.Types.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }
        private bool isExists(Guid id)
        {
            return _context.Types.Any(e => e.Id == id && !e.IsDeleted);
        }
    }

}

