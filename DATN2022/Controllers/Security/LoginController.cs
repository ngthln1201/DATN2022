﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using DATN2022.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using StackExchange.Redis;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace DATN2022.Controllers.Security
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IConfiguration _config;
        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        public LoginController(IConfiguration config, DATN2022DbContext context, IMapper mapper )
        {
            _config = config;
            _context = context;
            _mapper = mapper;
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Login([FromBody] AccountDto login)
        {
            if (!string.IsNullOrEmpty(login.Email) && !string.IsNullOrEmpty(login.PassWord))
            {
                var user = Authenticate(login);
                if (user != null)
                {
                    var token = Generate(user);
                    return Ok(token);
                }
            }
            return NotFound("User not found!");
        }

        private string Generate(UserLoginDto user)
        {
            var secirityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var crenditals = new SigningCredentials(secirityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Email),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.GivenName, user.FirstName),
                new Claim(ClaimTypes.Surname, user.LastName),
                new Claim(ClaimTypes.Role, user.Role)
            };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims,
                expires:DateTime.Now.AddMinutes(15),
                notBefore: DateTime.Now,
                signingCredentials:crenditals
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private UserLoginDto Authenticate(AccountDto login)
        {
            var currentUser= _context.CoachOwners.FirstOrDefault(x => x.Email == login.Email && x.PassWord == login.PassWord);
            if(currentUser != null)
            {
                return new UserLoginDto
                {
                    Id = currentUser.Id,
                    FirstName = currentUser.FirstName,
                    LastName = currentUser.LastName,
                    DateOfBirth = currentUser.DateOfBirth,
                    PhoneNumber = currentUser.PhoneNumber,
                    Email = currentUser.Email,
                    Nationality = currentUser.Nationality,
                    IdentityNo = currentUser.IdentityNo,
                    LisenseNo = currentUser.LisenseNo,
                    Address = currentUser.Address,
                    PassWord = currentUser.PassWord,
                    Role = currentUser.Role,
                    TenantId = currentUser.TenantId
                };
           
           
            }
            return null;
        }
    }
}
