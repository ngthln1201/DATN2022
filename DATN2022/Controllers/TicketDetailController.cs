﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketDetailController : ControllerBase
    {
        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        public TicketDetailController(IMapper mapper, DATN2022DbContext context)
        {

            _mapper = mapper;
            _context = context;
        }


  

        [HttpGet("{id}")]
        public async Task<ActionResult<TicketDetailDto>> GetById(Guid id)
        {
            var data = await _context.TicketDetails.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<TicketDetailDto>(data));
        }

        [HttpPost]
        public async Task<Object> Create(TicketDetailDto ticketdetails)
        {
            try
            {
                var entity = _mapper.Map<TicketDetail>(ticketdetails);
                var result = _context.TicketDetails.Add(entity);
                if (result != null)
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPut]
        public async Task<ActionResult> Update(TicketDetailDto ticketdetails)
        {
            var entity = _mapper.Map<TicketDetail>(ticketdetails);
            var result = _context.TicketDetails.Update(entity);
            if (result != null)
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    return NotFound();
                }

                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]

        public async Task<ActionResult<TicketDetail>> Delete(Guid id)
        {

            var data = await _context.TicketDetails.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                _context.TicketDetails.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }
    }
}
