﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using DATN2022.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DropOffController : ControllerBase
    {
        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        private readonly IRedisCacheService _cache;
        public DropOffController(IMapper mapper, DATN2022DbContext context, IRedisCacheService cache)
        {
            _cache = cache;
            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DropOffDto>>> GetAll([FromBody] string tenantId)
        {
            var datas = await _context.DropOffs.Where(x => !x.IsDeleted && x.TenantId.Equals(tenantId) && x.Status == 1).ToListAsync();
            if (datas.Count <= 0)
            {
                return NoContent();
            }
            return Ok(_mapper.Map<IEnumerable<DropOffDto>>(datas));

        }

        [HttpGet("{id}")]
        public ActionResult<DropOffDto> GetById(Guid id)
        {
            var cached = _cache.Get<DropOffDto>(id.ToString());
            if (cached != null) return cached;
            return cached == null ? NotFound() : Ok(_mapper.Map<DropOffDto>(cached));
        }

        [HttpPost]
        public async Task<Object> Create(DropOffDto dropOff)
        {
            try
            {
                var entity = _mapper.Map<DropOff>(dropOff);
                var result = _context.DropOffs.Add(entity);
                if (result != null)
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPut]
        public async Task<ActionResult> Update(DropOffDto dropOff)
        {
            var entity = _mapper.Map<DropOff>(dropOff);
            var result = _context.DropOffs.Update(entity);
            if (result != null)
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    if (!isExists(dropOff.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]

        public async Task<ActionResult<DropOff>> Delete(Guid id)
        {

            var data = await _context.DropOffs.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                _context.DropOffs.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }

        private bool isExists(Guid id)
        {
            return _context.DropOffs.Any(e => e.Id == id && !e.IsDeleted);
        }
    }
}
