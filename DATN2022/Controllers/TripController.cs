﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripController : ControllerBase
    {
  
        private readonly DATN2022DbContext _context;     
        private readonly IMapper _mapper;
        public TripController( IMapper mapper, DATN2022DbContext context)
        {
           
            _mapper = mapper;
           _context = context;    
        }
         [HttpGet]
        public async Task<ActionResult<IEnumerable<TripDto>>> GetAll()
        {
            var datas = await _context.Trips.Where(x=>!x.IsDeleted).ToListAsync();
            if (datas.Count == 0)
            {
                return NoContent();
            }
            return Ok(_mapper.Map<IEnumerable<TripDto>>(datas));
           
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TripDto>> GetById(Guid id)
        {
            var data = await _context.Trips.FindAsync(id);
            return data == null ? NotFound() : Ok(_mapper.Map<TripDto>(data));
        }
        [HttpPost]
        public async Task<Object> Create(TripDto trip)
        {
            try
            {
                if (!isDuplicate(trip.From, trip.To))
                {
                    var entity = _mapper.Map<Trip>(trip);
                    var result = _context.Trips.Add(entity);
                    if (result != null)
                    {
                        await _context.SaveChangesAsync();
                        return CreatedAtAction("GetById", new { id = entity.Id }, entity);
                    }
                    return BadRequest();
                }
                else
                {
                    var datas = _context.Trips.Where(x => !x.IsDeleted && x.From.Equals(trip.From) && x.To.Equals(trip.To));
                    return CreatedAtAction("GetById", new { id = datas }, datas);
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        [HttpPut]
        public async Task<ActionResult> Update(TripDto trip)
        {
            var entity = _mapper.Map<Trip>(trip);
            var result =  _context.Trips.Update(entity);
            if (result != null)
            {
                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    if (!isExists(trip.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
               
                return CreatedAtAction("GetById", new { id = entity.Id }, entity);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
       
        public async Task<ActionResult<TripDto>> Delete(Guid id)
        {
     
            var data = await _context.Trips.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                 _context.Trips.Remove(data);
                await _context.SaveChangesAsync();
                return NoContent();
            }
        }
        private bool isExists(Guid id)
        {
            return _context.Trips.Any(e => e.Id == id && !e.IsDeleted);
        }

        private bool isDuplicate(string from, string to)
        {
            return _context.Trips.Any(e => e.From == from && e.To == to && !e.IsDeleted);
        }
    }
}
