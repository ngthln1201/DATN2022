﻿using AutoMapper;
using DATN2022.Dtos;
using DATN2022.Models;

namespace DATN2022.Profiles
{
    public class AutoMapperProfileConfiguration:Profile
    {
        public AutoMapperProfileConfiguration()
        {
            CreateMap<CoachDto,Coach>();
            CreateMap<Coach, CoachDto>();
            CreateMap<TripDto, Trip>(); 
            CreateMap<Trip, TripDto>();
            CreateMap<CoachOwnerDto, CoachOwner>();
            CreateMap<CoachOwner, CoachOwnerDto>();
            CreateMap<DropOffDto, DropOff>();
            CreateMap<DropOff, DropOffDto>();
            CreateMap<LayoutDto, Layout>();
            CreateMap<Layout, LayoutDto>();
            CreateMap<TypeDto, Type>();
            CreateMap<Type, TypeDto>();
            CreateMap<TicketDto, Ticket>();
            CreateMap<Ticket, TicketDto>();
            CreateMap<PickupDto, Pickup>();
            CreateMap<Pickup, PickupDto>();
            CreateMap<TicketDetailDto, TicketDetail>();
            CreateMap<TicketDetail, TicketDetailDto>();
            CreateMap<OrderDto, Order>();
            CreateMap<Order, OrderDto>();
            CreateMap<SeatDto, Seat>();
            CreateMap<Seat, SeatDto>();
        }
    }
}
