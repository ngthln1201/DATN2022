﻿using DATN2022.Dtos.Request;
using System.Threading.Tasks;

namespace DATN2022.Services.Interfaces
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequest mailRequest);
    }
}
