﻿using DATN2022.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DATN2022.Services.Interfaces
{
    public interface IBServices
    {
        public Task<Object> saveToDb();
        public Task<BookingDto> saveBookingValueToRedis(BookingDto data);
    }
}
