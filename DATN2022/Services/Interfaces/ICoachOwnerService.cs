﻿namespace DATN2022.Services.Interfaces
{
    public interface ICoachOwnerService
    {
        public bool isDuplicateCoachOwner(string email);
    }
}
