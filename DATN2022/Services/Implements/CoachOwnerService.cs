﻿using Microsoft.EntityFrameworkCore;
using System.Net.Mail;
using System;
using DATN2022.DBContext;
using System.Linq;
using Microsoft.IdentityModel.Tokens;
using DATN2022.Services.Interfaces;

namespace DATN2022.Services.Implements
{
    public class CoachOwnerService :ICoachOwnerService
    {
        private readonly DATN2022DbContext _context;

        public void IsValidEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
            }
            catch (FormatException)
            {
                throw new Exception("Email is invalid: " + emailaddress);
            }
        }
        public bool isDuplicateCoachOwner(string email)
        {
            IsValidEmail(email);
            var data = _context.CoachOwners.Where(x => x.Email == email && x.IsDeleted == false && x.Status == 1);
            return data.IsNullOrEmpty() ? true : false;

        }
    }
}
