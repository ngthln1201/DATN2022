﻿using System;

namespace DATN2022.Services.Implements
{
    public class UserService
    {
        public string currentUser { get; set; } = Environment.UserName;
    }
}
