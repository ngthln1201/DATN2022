﻿using DATN2022.Services.Interfaces;
using StackExchange.Redis;
using System.Text.Json;

namespace DATN2022.Services.Implements
{
    public class RedisCacheService : IRedisCacheService
    {
        static readonly ConnectionMultiplexer _redis = ConnectionMultiplexer.Connect("redis-18398.c9.us-east-1-4.ec2.cloud.redislabs.com:18398,password=datn2022@");

        public RedisCacheService(  )
        {

        }
        public T Get<T>(string key)
        {
            var db = _redis.GetDatabase(0);          
            var value = db.StringGet(key);
            if (value != "")
            {
                return JsonSerializer.Deserialize<T>(value);
            }
            return default;
        }
        public T Set<T>(string key, T value)
        {
            var db = _redis.GetDatabase(0);
     
            db.StringSet(key, JsonSerializer.Serialize(value));

            return value;
        }
        public object RemoveData(string key)
        {
            var db = _redis.GetDatabase(0); 
            bool _isKeyExist = db.KeyExists(key);
            if (_isKeyExist == true)
            {
                return db.KeyDelete(key);
            }
            return false;
        }
    }
}
