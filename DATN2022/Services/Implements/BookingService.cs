﻿using AutoMapper;
using DATN2022.Dtos;
using DATN2022.Models;
using DATN2022.Repository.Interfaces;
using DATN2022.Services.Interfaces;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.Json.Nodes;
using System.Threading.Tasks;

namespace DATN2022.Services.Implements
{
    public class BookingService : IBServices
    {
        private readonly IBookingRepository _repo;
        private readonly IRedisCacheService _cache;
        private readonly IMapper _mapper;
        public BookingService(IRedisCacheService cache, IBookingRepository repo, IMapper mapper)
        {
            _cache = cache;
            _repo = repo;
            _mapper = mapper;
        }

        public Task<object> saveToDb()
        {
            Order cache = ifOrderCacheExistThenReturn();
            return (!cache.Equals(null) && cache.PayStatus == "00") ? _repo.saveToDatabase() : default;
        }
        public async Task<BookingDto> saveBookingValueToRedis(BookingDto data)
        {
            detailTicketRedisSave(data.TicketDetail);
            ticketRedisSave(data.Ticket);
            data.TicketDetail.ForEach(x =>
            {
                _repo.lockSeatById(x.SeatId);
            });

           await Task.Delay(30000).ContinueWith(t =>
            {
               data.TicketDetail.ForEach( x =>
                {
                    _repo.unLockSeatById(x.SeatId);
                });
            });

            return  data;
        }
        public TicketDto ticketRedisSave(TicketDto item)
        {
            ifTicketCacheDoesNotExist();
            TicketDto ticket = new TicketDto();
            if (verifyCacheDetailExistOrNot())
            {
               var listDetail = _cache.Get<List<TicketDetail>>("ticketdetail");
                 ticket = new TicketDto
                {
                    Id = listDetail.ElementAt(0).TicketId,
                    CustomerName = item.CustomerName,
                    CustomerAddress = item.CustomerAddress,
                    CustomerAge = item.CustomerAge,
                    CustomerPhone = item.CustomerPhone,
                    CustomerEmail = item.CustomerEmail,
                    CustomerGender = item.CustomerGender,
                    IdentityCertificate = item.IdentityCertificate,
                    TotalPaymentAmount = item.TotalPaymentAmount,
                    BookedAt = DateTime.Now,
                };
            }
            TicketDto cache = _cache.Set<TicketDto>("ticket", ticket);
            if(cache == null)
            {
                Console.WriteLine("Vui lòng kiểm tra thông tin xe");
            }
           
            return cache;
        }
     
        public List<TicketDetailDto> detailTicketRedisSave(List<TicketDetailDto> ticketDetails)
        {
            ifTicketDetailCacheDoesNotExist();
            List<TicketDetail> cacheDetailDatas = new List<TicketDetail>();
            Guid newId = Guid.NewGuid();
            ticketDetails.ForEach(x =>
            {
                TicketDetail dt = new TicketDetail
                {
                    Id = Guid.NewGuid(),
                    TicketNo = x.TicketNo,
                    SeatNo = x.SeatNo,
                    PickUpId = x.PickUpId,
                    DropOffId = x.DropOffId,
                    SeatId = x.SeatId,
                    TicketId = newId,
                };
                cacheDetailDatas.Add(dt);
            });

            _cache.Set<List<TicketDetail>>("ticketdetail", cacheDetailDatas);

            return _mapper.Map<List<TicketDetailDto>>(cacheDetailDatas);
        }

        private  void ifTicketDetailCacheDoesNotExist()
        {
            try
            {
                _cache.Get<List<TicketDetail>>("ticketdetail");

            }
            catch (Exception)
            {
                List<TicketDetail> td = new List<TicketDetail> { };
                _cache.Set<List<TicketDetail>>("ticketdetail", td);
            }
        }
        private void ifTicketCacheDoesNotExist()
        {
            try
            {
                _cache.Get<TicketDto>("ticket");
            }
            catch (Exception)
            {
                TicketDto ticket = new TicketDto();
                _cache.Set<TicketDto>("ticket", ticket);
            }
        }
        private bool verifyCacheDetailExistOrNot()
        {
            try
            {
                return _cache.Get<List<TicketDetail>>("ticketdetail").IsNullOrEmpty()?false:true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private Order ifOrderCacheExistThenReturn()
        {
            try
            {
                Order order = _cache.Get<Order>("ordermodel");
                if(order != null) return order;
            }
            catch
            {
                Console.WriteLine("Vui lòng kiểm tra thông tin thanh toán!");
            }
            return null;
        }
    }
}
