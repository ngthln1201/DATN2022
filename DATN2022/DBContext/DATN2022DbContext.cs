﻿using DATN2022.Models;
using Microsoft.EntityFrameworkCore;
using System;
namespace DATN2022.DBContext
{
    public class DATN2022DbContext : DbContext
    {
        public DbSet<CoachOwner> CoachOwners { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<Pickup> Pickups { get; set; }
        public DbSet<DropOff> DropOffs { get; set; }
        public DbSet<Models.Type> Types { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TicketDetail> TicketDetails { get; set; }
        public DbSet<Layout> Layouts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Tenant> Tenants { get; set; }

        public DATN2022DbContext(DbContextOptions<DATN2022DbContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CoachOwner>().HasData(new CoachOwner
            {
                Id = Guid.Parse("8909d3b5-21d7-4dd0-b04a-fa6599dbbc14"),
                FirstName = "Uncle",
                LastName = "Bob",
                Email = "uncle.bob@gmail.com",
                DateOfBirth = new DateTime(1979, 04, 25),
                PhoneNumber = "999-888-7777",
                Nationality = "Vietnam",
                IdentityNo = "034201003512",
                LisenseNo = "123456789012",
                Address = "Hà Nội",
                PassWord = "123456",
                Role = "admin",
                IsDeleted = false,
            }, new CoachOwner
            {
                Id = Guid.Parse("25a05905-43dc-4210-97c0-a929742c8e9d"),
                FirstName = "Vital",
                LastName = "Water",
                Email = "vital.bob@gmail.com",
                DateOfBirth = new DateTime(1920, 05, 15),
                PhoneNumber = "666-123-2345",
                Nationality = "Vietnam",
                IdentityNo = "056201003512",
                LisenseNo = "123456789012",
                Address = "Hà Nội",
                PassWord = "123456",
                Role = "admin",
                IsDeleted = false,
            });

            modelBuilder.Entity<Models.Type>().HasData(new Models.Type
            {
                Id = Guid.Parse("ac144359-c5ca-49f2-89fc-d7173d7d051b"),
                Name = "Limousine",
                Seats = 16,
                IsDeleted = false,
                LayoutId = Guid.Parse("50aa2d9f-76f6-4075-a0d2-2dcecb6246bb")
            }, new Models.Type
            {
                Id = Guid.Parse("ee70e6d6-848a-4366-b340-72cafd257c01"),
                Name = "Limousine",
                Seats = 16,
                IsDeleted = false,
                LayoutId = Guid.Parse("24e30892-5969-4b12-a586-826081665961")

            });
            modelBuilder.Entity<Coach>().HasData(new Coach
            {
                Id = Guid.Parse("26daa044-5071-4750-856b-1e59595fca5a"),
                DepartureTime = DateTime.Today,
                LisencePlates = "BKAV-1012",
                Reserve = true,
                Color = "đỏ",
                Price = 200.000,
                CoachOwnerId = Guid.Parse("8909d3b5-21d7-4dd0-b04a-fa6599dbbc14"),
                TripId = Guid.Parse("89b6d753-2096-4d6a-90ab-7ee3b1a7559c"),       
                IsDeleted = false,
                TypeId= Guid.Parse("ac144359-c5ca-49f2-89fc-d7173d7d051b")
            }, new Coach
            {
                Id = Guid.Parse("5fd3023d-5cd9-4b0d-8210-c2e1c3e4f650"),
                DepartureTime = DateTime.Today,
                LisencePlates = "CNL-1019",
                Reserve = false,
                Color = "đen",
                Price = 200.000,
                CoachOwnerId = Guid.Parse("8909d3b5-21d7-4dd0-b04a-fa6599dbbc14"),
                TripId = Guid.Parse("89b6d753-2096-4d6a-90ab-7ee3b1a7559c"),
                IsDeleted = false,
                TypeId = Guid.Parse("ac144359-c5ca-49f2-89fc-d7173d7d051b")
            });

            modelBuilder.Entity<Trip>().HasData(new Trip
            {
                Id = Guid.Parse("89b6d753-2096-4d6a-90ab-7ee3b1a7559c"),
                From = "Hà Nội",
                To = "Thái Bình",
                TripNo = "001",
                IsDeleted = false
            },
            new Trip
            {
                Id = Guid.Parse("5b9ad1b4-fa5f-4727-8c83-ef767e6daf73"),
                From = "Hà Nội",
                To = "Bắc Giang",
                TripNo = "002",
                IsDeleted = false
            });

            modelBuilder.Entity<Layout>().HasData(new Layout
            {
                Id = Guid.Parse("50aa2d9f-76f6-4075-a0d2-2dcecb6246bb"),
                Name = "16 cơ bản",
                Value = "144445",
                NumberOfDeckers = 1
            }, new Layout
            {
                Id = Guid.Parse("24e30892-5969-4b12-a586-826081665961"),
                Name = "32 cơ bản",
                Value = "14444445",
                NumberOfDeckers = 1
            });
     
            modelBuilder.Entity<Pickup>().HasData(new Pickup
            {
                Id = Guid.Parse("1f3bcc61-73ef-44d2-bbe8-fae9be4ff012"),
                PhoneNumber = "0376121223",
                Address = "thôn cù lao, xã cù lỗ, huyện cù lôn, tỉnh sóc trăng",
                CoachId = Guid.Parse("26daa044-5071-4750-856b-1e59595fca5a"),
                IsDeleted = false
            }, new Pickup
            {
                Id = Guid.Parse("c6ac8db4-932c-41ce-a3b2-c61fb96c40a3"),
                PhoneNumber = "0376121224",
                Address = "thôn cù lù, xã lỗ lỗ, huyện cù lôn, tỉnh bà rịa",
                CoachId = Guid.Parse("5fd3023d-5cd9-4b0d-8210-c2e1c3e4f650"),
                IsDeleted = false
            });

            modelBuilder.Entity<DropOff>().HasData(new DropOff
            {

                Id = Guid.Parse("114c8ed3-9242-43dc-8208-531402c5ada0"),
                PhoneNumber = "0376121223",
                Address = "thôn cù lao, xã cù lỗ, huyện cù lôn, tỉnh sóc trăng",
                CoachId = Guid.Parse("26daa044-5071-4750-856b-1e59595fca5a")
            }, new DropOff
            {
                Id = Guid.Parse("40e22896-ca05-4395-aa85-e0c4f4d82d8c"),
                PhoneNumber = "0376121224",
                Address = "thôn cù lù, xã lỗ lỗ, huyện cù lôn, tỉnh bà rịa",
                CoachId = Guid.Parse("5fd3023d-5cd9-4b0d-8210-c2e1c3e4f650"),
            });

           

            modelBuilder.Entity<Seat>().HasData(new Seat
            {
                Id = Guid.Parse("a2e214fb-f507-4cd4-951f-e998e9d490a3"),
                SeatNo = 1,
                Status = 0,
                Row = "một",
                Column = "hai",
                CustomPrice = 100.000,
                CoachId = Guid.Parse("26daa044-5071-4750-856b-1e59595fca5a")
            }, new Seat
            {
                Id = Guid.Parse("2f649857-1f77-4d57-9d61-e9792c59c3d8"),
                SeatNo = 2,
                Status = 0,
                Row = "một",
                Column = "một",
                CustomPrice = 150.000,
                CoachId = Guid.Parse("5fd3023d-5cd9-4b0d-8210-c2e1c3e4f650")
            });

            modelBuilder.Entity<TicketDetail>().HasData(new TicketDetail
            {
                Id = Guid.Parse("e2214f5a-340d-4232-9a78-52c02e0c4b72"),
                SeatNo = 1,
                TicketNo = "1",
                SeatId = Guid.Parse("a2e214fb-f507-4cd4-951f-e998e9d490a3"),
                TicketId = Guid.Parse("dc9419c0-8a44-44b2-ba93-5ca823326b26"),
                PickUpId = Guid.Parse("1f3bcc61-73ef-44d2-bbe8-fae9be4ff012"),
                DropOffId = Guid.Parse("114c8ed3-9242-43dc-8208-531402c5ada0"),
               
            }, new TicketDetail
            {
                Id = Guid.Parse("606280d0-f01f-4150-b25f-c5f3d0848a6e"),
                SeatNo = 2,
                SeatId = Guid.Parse("2f649857-1f77-4d57-9d61-e9792c59c3d8"),
                TicketId = Guid.Parse("94d713f3-49fa-4c96-ac84-10af1ca49ee3"),
                DropOffId = Guid.Parse("40e22896-ca05-4395-aa85-e0c4f4d82d8c"),
                PickUpId = Guid.Parse("7f0483a5-7c87-4897-9229-038cb35f5a85"),
                TicketNo = "2",
            });

            modelBuilder.Entity<Ticket>().HasData(new Ticket
            {
                Id = Guid.Parse("dc9419c0-8a44-44b2-ba93-5ca823326b26"),
                Status = 0,
                Quantity = 2,
                CustomerName = "Anh Vũ",
                CustomerAddress = "Nam Từ Liêm - Hà Nội",
                CustomerAge = "19",
                CustomerEmail = "vuda1@fpt.edu.vn",
                CustomerGender = "Nam",
                CustomerPhone = "0347538963",
                IdentityCertificate = "Căn cước công dân",
            },
            new Ticket
            {
                Id = Guid.Parse("94d713f3-49fa-4c96-ac84-10af1ca49ee3"),
                Status = 2,
                CustomerName = "Em Vũ",
                CustomerAddress = "Mỹ Đình - Hà Nội",
                CustomerAge = "23",
                CustomerEmail = "hehehehihi@fpt.edu.vn",
                CustomerGender = "Nữ",
                CustomerPhone = "0347538645",
                IdentityCertificate = "Căn cước công dân",

            }); ;

            modelBuilder.Entity<Order>().HasData(new Order
            {
                Id = Guid.Parse("0e05f35e-53fb-4469-a767-26530175d5e4"),
                OrderNo = 1,
                Amount = 1,
                OrderDesc = "Tăng",
                CreatedDate = DateTime.Today,
                Status = "hoạt động",
                PaymentTranId = 1001001,
                BankCode = "01101100",
                PayStatus = "Thành công",
                TicketId = Guid.Parse("dc9419c0-8a44-44b2-ba93-5ca823326b26")
            },
            new Order
           {
                Id = Guid.Parse("715b2871-4c37-4b89-a25f-409ad2433536"),
                OrderNo = 1,
                Amount = 1,
                OrderDesc = "Tăng",
                CreatedDate = DateTime.Today,
                Status = "hoạt động",
                PaymentTranId = 1001001,
                BankCode = "01101100",
                PayStatus = "Thành công",
                TicketId = Guid.Parse("94d713f3-49fa-4c96-ac84-10af1ca49ee3")
            });
            modelBuilder.Entity<Tenant>().HasData(new Tenant
            {
                Id = Guid.Parse("b25bbb5e-2eb6-4082-bc3f-5c7313924dad"),
                Name = "system",
                TID = DateTime.Now.Ticks.ToString(),
                ConnectionString = "",
                Status = 1
            }, new Tenant
            {
                Id = Guid.Parse("c67b96df-4da3-4d4b-b9f8-d27a1e9b3315"),
                Name = "system",
                TID = DateTime.Now.Ticks.ToString(),
                ConnectionString = "",
                Status = 1
            }); 
        }

    }
}
