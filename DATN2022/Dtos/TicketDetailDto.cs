﻿using System;

namespace DATN2022.Dtos
{
    public class TicketDetailDto
    {
        public Guid Id { get; set; }
        public string TicketNo { get; set; }
        public Guid TicketId { get; set; }
        public int SeatNo { get; set; }
        public Guid PickUpId { get; set; }
        public Guid DropOffId { get; set; }
        public Guid SeatId { get; set; }
        public string TenantId { get; set; }
    }
}
