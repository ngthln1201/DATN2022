﻿using System;

namespace DATN2022.Dtos
{
    public class TenantDto
    {
        public Guid Id { get; set; } = new Guid();
        public string Name { get; set; }
        public string TID { get; set; }
        public string ConnectionString { get; set; }
        public int Status { get; set; }

        public TenantDto()
        {
        }

        public TenantDto(string name, string tID, string connectionString, int status)
        {
            Name = name;
            TID = tID;
            ConnectionString = connectionString;
            Status = status;
        }
    }
}
