﻿using System;

namespace DATN2022.Dtos
{
    public class TicketDto
    {
        public Guid Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerAge { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerGender { get; set; }
        public string IdentityCertificate { get; set; }
        public float TotalPaymentAmount { get; set; }
        public DateTime BookedAt { get; set; }
        public string TenantId { get; set; }
        public int Status { get; set; }
    }
}
