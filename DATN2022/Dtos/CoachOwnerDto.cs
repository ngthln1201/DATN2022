﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections;
using System.Collections.Generic;

namespace DATN2022.Dtos
{
    public class CoachOwnerDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Nationality { get; set; }
        public string IdentityNo { get; set; }
        public string LisenseNo { get; set; }
        public string Address { get; set; }
        public string TenantId { get; set; }
        public int Status { get; set; }

        public CoachOwnerDto()
        {

        }

        public CoachOwnerDto(Guid id, string firstName, string lastName, DateTime? dateOfBirth, string phoneNumber, string email, 
            string nationality, string identityNo, string lisenseNo, string address, string tenantId, int status)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            DateOfBirth = dateOfBirth;
            PhoneNumber = phoneNumber;
            Email = email;
            Nationality = nationality;
            IdentityNo = identityNo;
            LisenseNo = lisenseNo;
            Address = address;
            TenantId = tenantId;
            Status = status;
        }
    }

}
