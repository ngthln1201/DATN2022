﻿using DATN2022.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DATN2022.Dtos
{
    public class DropOffDto
    {
        public Guid Id { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public Guid CoachId { get; set; }
        public string TenantId { get; set; }
        public int Status { get; set; }

        public DropOffDto()
        {
        }

        public DropOffDto(Guid id, string phoneNumber, string address, Guid coachId, string tenantId, int status)
        {
            Id = id;
            PhoneNumber = phoneNumber;
            Address = address;
            CoachId = coachId;
            TenantId = tenantId;
            Status = status;
        }
    }
}
