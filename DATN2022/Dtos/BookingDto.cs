﻿using System.Collections.Generic;

namespace DATN2022.Dtos
{
    public class BookingDto
    {
        public TicketDto Ticket { get; set; }  
        public List<TicketDetailDto> TicketDetail { get; set; }
    }
}
