﻿using System;

namespace DATN2022.Dtos
{
    public class PickupDto
    {
        public Guid Id { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public Guid CoachId { get; set; }
        public string TenantId { get; set; }
        public int Status { get; set; }

        public PickupDto(Guid id, string phoneNumber, string address, Guid coachId)
        {
            Id = id;
            PhoneNumber = phoneNumber;
            Address = address;
            CoachId = coachId;
        }
    }
}
