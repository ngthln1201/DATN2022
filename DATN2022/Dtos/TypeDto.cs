﻿using System;

namespace DATN2022.Dtos
{
    public class TypeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Seats { get; set; }
        public Guid LayoutId { get; set; }
    }
}
