﻿using System;

namespace DATN2022.Dtos
{
    public class OrderDto
    {
        public Guid Id { get; set; }
        public long OrderNo { get; set; }
        public long Amount { get; set; }
        public string OrderDesc { get; set; }
        public DateTime CreatedDate { get; set; }
        public long PaymentTranId { get; set; }
        public string BankCode { get; set; }
        public string PayStatus { get; set; }
        public Guid TicketId { get; set; }
        public string TenantId { get; set; }
        public int Status { get; set; }
    }
}
