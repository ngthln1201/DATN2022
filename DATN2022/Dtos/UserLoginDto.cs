﻿using System;

namespace DATN2022.Dtos
{
    public class UserLoginDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Nationality { get; set; }
        public string IdentityNo { get; set; }
        public string LisenseNo { get; set; }
        public string Address { get; set; }
        public string PassWord { get; set; }
        public string Role { get; set; }
        public string TenantId { get; set; }
        public int Status { get; set; }
    }
}
