﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace DATN2022.Models
{
    public class SeatDto
    {
        public Guid Id { get; set; }
        public int SeatNo { get; set; }
        public string Row { get; set; }
        public string Column { get; set; }
        public double CustomPrice { get; set; }
        public Guid CoachId { get; set; }
        public int Status { get; set; }

    }
}
