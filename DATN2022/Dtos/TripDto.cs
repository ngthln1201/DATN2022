﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace DATN2022.Models
{
    public class TripDto
    {
        public Guid Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string TripNo { get; set; }
        public TripDto(Guid id, string from, string to, string tripNo)
        {
            this.Id = id;
            this.From = from;
            this.To = to;
            this.TripNo = tripNo;
        }
    }   
}
