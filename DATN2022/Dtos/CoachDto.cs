﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using DATN2022.Models;

namespace DATN2022.Dtos
{
    public class CoachDto
    {
        public Guid Id { get; set; }
        public DateTime DepartureTime { get; set; }
        public string LisencePlates { get; set; }
        public bool Reserve { get; set; }
        public string Color { get; set; }
        public double Price { get; set; }
        //public byte Image { get; set; }
        public Guid TripId { get; set; }
        public Guid TypeId { get; set; }
        public bool isPrepayment { get; set; } 
        public Guid CoachOwnerId { get; set; }
        public string TenantId { get; set; }
        public int Status { get; set; }
        public CoachDto()
        {

        }

        public CoachDto(Guid id, DateTime departureTime, string lisencePlates, bool reserve, string color, double price, Guid tripId,
            Guid typeId, bool isPrepayment, Guid coachOwnerId, string tenantId, int status)
        {
            Id = id;
            DepartureTime = departureTime;
            LisencePlates = lisencePlates;
            Reserve = reserve;
            Color = color;
            Price = price;
            TripId = tripId;
            TypeId = typeId;
            this.isPrepayment = isPrepayment;
            CoachOwnerId = coachOwnerId;
            TenantId = tenantId;
            Status = status;
        }
    }
}
