﻿using System;
using System.Collections.Generic;

namespace DATN2022.Dtos.Request
{
    public class CreateCoachDto
    {
        //coach
        public Guid Id { get; set; }
        public DateTime DepartureTime { get; set; }
        public string LisencePlates { get; set; }
        public bool Reserve { get; set; }
        public string Color { get; set; }
        public double Price { get; set; }
        public Guid TripId { get; set; }
        public Guid TypeId { get; set; }
        public bool isPrepayment { get; set; }
        public Guid CoachOwnerId { get; set; }
        public string TenantId { get; set; }
        //trip
        public string From { get; set; }
        public string To { get; set; }
        //pickup
        public List<PickupDto> pickups { get; set; }
        //dropoff
        public List<DropOffDto> dropOffs { get; set; }
    }
}
