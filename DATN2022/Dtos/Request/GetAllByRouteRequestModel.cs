﻿using System;

namespace DATN2022.Dtos.Request
{
    public class GetAllByRouteRequestModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public DateTime Departuretime { get; set; }
    }
}
