﻿using System;

namespace DATN2022.Dtos.Request
{
    public class GetCoachDetailsRequest
    {
        public Guid CoachId { get; set; }
    }
}
