﻿using System;

namespace DATN2022.Dtos
{
    public class LayoutDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int NumberOfDeckers { get; set; }
    }
}
