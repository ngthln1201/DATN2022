﻿using AutoMapper;
using DATN2022.DBContext;
using DATN2022.Dtos;
using DATN2022.Dtos.Request;
using DATN2022.Models;
using DATN2022.Repository.Interfaces;
using DATN2022.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2022.Repository.Implements
{
    public class BookingRepository : IBookingRepository
    {
        private readonly DATN2022DbContext _context;
        private readonly IMapper _mapper;
        private readonly IRedisCacheService _cache;
        private readonly IMailService _mail;

        public BookingRepository(DATN2022DbContext context, IMapper mapper, IRedisCacheService cache, IMailService mail )
        {
            _context = context;
            _mapper = mapper;
            _cache = cache;
            _mail = mail;
        }
        public async Task<object> saveToDatabase()
        {
            try
            {
                TicketDto ticketCache = _cache.Get<TicketDto>("ticket");
                List<TicketDetailDto> detailTicketCache = _cache.Get<List<TicketDetailDto>>("ticketdetail");
     
                Ticket ticket = _mapper.Map<Ticket>(ticketCache);
                _context.Tickets.Add(ticket);
                List<string> ticketNo = new List<string>();
                List<string> seatNo = new List<string>();
                foreach (TicketDetailDto d in detailTicketCache)
                {
                    var v = _mapper.Map<TicketDetail>(d);
                     _context.TicketDetails.Add(v);
                    try
                    {
                        lockSeatById(d.SeatId);
                    }
                    catch(NullReferenceException ex)
                    {
                        Console.WriteLine($"Ghế {d.SeatNo} đã được đặt, vui lòng thử lại!" + ex);
                    }
                   
                    ticketNo.Add(d.TicketNo);
                    seatNo.Add(d.SeatNo.ToString());
                }
               string infoTicketNo = string.Join(",", ticketNo);
               string infoSeatNo = string.Join(",", seatNo);
               
                Console.WriteLine("Start send mail to {0}", ticket.CustomerEmail);
                MailRequest mail = new MailRequest();
                mail.ToEmail = ticket.CustomerEmail;
                mail.Subject = $"Thông tin đặt vé {ticket.Id}";
                mail.Body = $"Qúy khách đã đặt {ticket.Quantity} vé xe khách vào lúc: {ticket.BookedAt} " +
                        $"Mã vé: {ticketNo}" +
                        $"Mã ghế: {seatNo}";
                await _mail.SendEmailAsync(mail);
                _cache.RemoveData("ticket");
                _cache.RemoveData("ticketdetail");
                _cache.RemoveData("ordermodel");
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        public void lockSeatById(Guid id)
        {
            var seat = _context.Seats.Where(x => x.Id == id && x.IsDeleted == false && x.Status != 0).FirstOrDefault();
            if(seat != null)seat.Status = 0;
            _context.Seats.Update(seat);
            _context.SaveChanges();
        }
        public async void unLockSeatById(Guid id)
        {
             var seat = await _context.Seats.Where(x => x.Id == id && x.IsDeleted == false && x.Status != 1).FirstOrDefaultAsync();
            if (seat != null)seat.Status = 1;
            _context.Seats.Update(seat);
            await _context.SaveChangesAsync();
        }
    }
}
