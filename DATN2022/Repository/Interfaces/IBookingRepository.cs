﻿using DATN2022.Dtos;
using System;
using System.Threading.Tasks;

namespace DATN2022.Repository.Interfaces
{
    public interface IBookingRepository
    {
        public Task<object> saveToDatabase();
        public void lockSeatById(Guid id);
        public void unLockSeatById(Guid id);
    }
}
