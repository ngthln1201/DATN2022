﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace DATN2022.Models
{
    public class Ticket
    {
        [Key]
        public Guid Id { get; set; }
        public int  Quantity { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerAge { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerGender { get; set; }
        public string IdentityCertificate { get; set; }
        public DateTime BookedAt { get; set; }
        public string TenantId { get; set; }
        public int Status { get; set; } = 1;
        //khóa ngoại
        public virtual Order Order { get; set; }
        //bảng con
        public virtual ICollection<TicketDetail> TicketDetails { get; set; }
    }
}
