﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace DATN2022.Models
{
    public class TicketDetail
    {
        
        [Key]
        public Guid Id { get; set; }
        public string TicketNo { get; set; }
        
        public int SeatNo { get; set; }
        public Guid DropOffId { get; set; } 
        public Guid PickUpId { get; set; }
        public string TenantId { get; set; }
        public int Status { get; set; } = 1;

        //khóa ngoại
        [ForeignKey("SeatId")]
        public Guid SeatId { get; set; }
        public virtual Seat Seat { get; set; }
        public Guid TicketId { get; set; }
        [ForeignKey("TicketId")]
        public Ticket Ticket { get; set; }
    
       
    }
}
