﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace DATN2022.Models
{
    public class Coach
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime DepartureTime { get; set; }
        public string LisencePlates { get; set; }
        public bool Reserve { get; set; }
        public string Color { get; set; }
        public double Price { get; set; }
        //public byte Image { get; set; }
        public bool isPrepayment { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public string TenantId { get; set; }
        public int Status { get; set; }

        //khóa ngoại 
        public Guid CoachOwnerId { get; set; }
        [ForeignKey("CoachOwnerId")]
        public CoachOwner CoachOwner { get; set; }
        public Guid TripId { get; set; }
        [ForeignKey("TripId")]
        //[System.Text.Json.Serialization.JsonIgnore]
        public Trip Trip { get; set; }
        public Guid TypeId { get; set; }
        [ForeignKey("TypeId")]
        public Type Type { get; set; }

        //bảng con
     
        public virtual ICollection<Pickup> Pickups { get; set; }
      
        public virtual ICollection<DropOff> DropOffs { get; set; }
    
        public virtual ICollection<Seat> Seats { get; set; }

    }
}
