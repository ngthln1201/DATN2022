﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DATN2022.Models
{
    public class Order
    {
        [Key]
        public Guid Id { get; set; }
        public long OrderNo { get; set; }
        public long Amount { get; set; }
        public string OrderDesc { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Status { get; set; }
        public long PaymentTranId { get; set; }
        public string BankCode { get; set; }
        public string PayStatus { get; set; }
        public string TenantId { get; set; }

        //khóa ngoại

        [ForeignKey("TicketId")]
        public Guid TicketId { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}
