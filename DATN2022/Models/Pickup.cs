﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DATN2022.Models
{
    public class Pickup
    {
        [Key]
        public Guid Id { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public bool IsDeleted { get; set; } = false;
        public string TenantId { get; set; }
        public int Status { get; set; }


        //khóa ngoại
        public Guid CoachId { get; set; }
        [ForeignKey("CoachId")]
        public Coach Coach { get; set; }
    }
}
