﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace DATN2022.Models
{
    public class Seat
    {
        [Key]
        public Guid Id { get; set; }
        public int SeatNo { get; set; }
        public int Status { get; set; }
        public string Row { get; set; }
        public string Column { get; set; }
        public double CustomPrice { get; set; }
        public bool IsDeleted { get; set; } = false;

        //khóa ngoại
        public virtual TicketDetail TicketDetail { get; set; }
        public Guid CoachId { get; set; }

        [ForeignKey("CoachId")]
        public Coach Coach { get; set; }
    }
}
