﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections;
using System.Collections.Generic;

namespace DATN2022.Models
{
    public class CoachOwner
    {
        [Key]
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Nationality { get; set; }
        public string IdentityNo { get; set; }
        public string LisenseNo { get; set; }
        public string Address { get; set; }
        public string PassWord { get; set; }
        public string Role { get; set; }
        public string TenantId { get; set; }
        public bool IsDeleted { get; set; } = false;
        public int Status { get; set; }

        //bảng con
        public virtual ICollection<Coach> Coaches { get; set; }

    }
}
