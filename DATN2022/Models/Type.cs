﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace DATN2022.Models
{
    public class Type
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Seats { get; set; }
        public bool IsDeleted { get; set; } = false;

        //khóa ngoại
        [ForeignKey("LayoutId")]
        public Guid LayoutId { get; set; }
        public virtual Layout Layout { get; set; }

        //bảng con
        public virtual ICollection<Coach> Coaches { get; set; }
    }
}
