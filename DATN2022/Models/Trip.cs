﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace DATN2022.Models
{
    public class Trip
    {
        [Key]
        public Guid Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string TripNo { get; set; }
        public bool IsDeleted { get; set; } = false;

        //bảng con
        public virtual ICollection<Coach> Coachs { get; set; }
    }   
}
