﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DATN2022.Models
{
    public class Layout
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int NumberOfDeckers { get; set; }
        public bool IsDeleted { get; set; } = false;

        //khóa ngoại
        public virtual Type Type { get; set; }
    }
}
